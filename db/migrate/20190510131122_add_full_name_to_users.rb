class AddFullNameToUsers < ActiveRecord::Migration[5.2]
  def up
    unless column_exists? :users, :full_name
      add_column :users, :full_name, :string, limit: 256, after: :email
    end
  end

  def down
    if column_exists? :users, :full_name
      remove_column :users, :full_name
    end
  end
end
