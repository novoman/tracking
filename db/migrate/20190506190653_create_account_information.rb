class CreateAccountInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :account_information do |t|
      t.decimal :sum, default: 0

      t.timestamps
    end

    add_reference :account_information, :user, foreign_key: { on_delete: :cascade }
  end
end
