class AddCategoryTypeValues < ActiveRecord::Migration[5.2]
  def up
    CategoryType.where(name: 'Income').first_or_create!(name: 'Income')
    CategoryType.where(name: 'Expenses').first_or_create!(name: 'Expenses')
  end

  def down
    CategoryType.where(name: 'Income').delete_all
    CategoryType.where(name: 'Expenses').delete_all
  end
end
