class CreateMoneyOperations < ActiveRecord::Migration[5.2]
  def change
    create_table :money_operations do |t|
      t.decimal :sum, default: 0
      t.string :comment, default: ''
      t.timestamp :operation_date, default: -> { 'CURRENT_TIMESTAMP' }
      t.index :operation_date

      t.timestamps
    end

    add_reference :money_operations, :category, foreign_key: { on_delete: :cascade }
  end
end
