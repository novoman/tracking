class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name, null: false, default: ''
      t.index :name

      t.timestamps
    end

    add_reference :categories, :user, foreign_key: { on_delete: :cascade }
    add_reference :categories, :category_type, foreign_key: { on_delete: :cascade }
  end
end
