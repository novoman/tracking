class CreateCategoryTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :category_types do |t|
      t.string :name, null: false, default: ''
      t.index :name

      t.timestamps
    end
  end
end
