class AddUniqueIndexToCategoty < ActiveRecord::Migration[5.2]
  def change
    add_index(:categories, %i[name category_type_id], unique: true)
  end
end
