# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w[
  tracking.css
  signin.css
  magnific-popup/magnific-popup.css
  jquery-rotator/style.css
  fontawesome-free/css/all.min.css
  datepicker/css/mdDateTimePicker.css
  jquery-easing/jquery.easing.min.js
  jquery-rotator/script.js
  magnific-popup/jquery.magnific-popup.min.js
  datepicker/js/moment.min.js
  datepicker/js/scroll-into-view-if-needed.min.js
  datepicker/js/draggabilly.pkgd.min.js
  datepicker/js/mdDateTimePicker.js
  money-operations-table.js
  category-tables.js
  tracking.js
]
