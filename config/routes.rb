Rails.application.routes.draw do
  root 'main#index'
  get 'main/index'

  namespace :category do
    post 'ajax_add_category'
    post 'ajax_edit_category'
    post 'ajax_delete_category'
  end

  namespace :money_operation do
    get 'export'
    get 'income_chart'
    get 'expenses_chart'
    post 'render_main_table'
    post 'render_add_modal'
    post 'render_edit_modal'
    post 'render_chart_modal'
    post 'ajax_add_money_operation'
    post 'ajax_edit_money_operation'
    post 'ajax_delete_money_operation'
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
end
