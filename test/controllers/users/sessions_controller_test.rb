require 'test_helper'
require 'json'

class SessionsControllerTest < ActionDispatch::IntegrationTest

  test '#create should login' do
    post user_session_url, params: { user: user_correct_params }
    assert_response :success
    assert_equal 'Signed in successfully.', flash[:success]
  end

  test '#create should not login' do
    post user_session_url, params: { user: user_incorrect_params }
    assert_response :unauthorized
    assert_equal ['Invalid email or password.'], JSON.parse(response.body)
  end

  private

  def user_correct_params
    user = {}
    user[:email] = 'test@test.com'
    user[:password] = '111111'

    user
  end

  def user_incorrect_params
    user = {}
    user[:email] = '1@test.com'
    user[:password] = '222'

    user
  end

end
