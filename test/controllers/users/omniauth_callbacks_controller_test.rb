require 'test_helper'
require 'json'

class OmniauthCallbacksControllerTest < ActionDispatch::IntegrationTest

  def setup
    OmniAuth.config.test_mode = true
  end

  test '#google_oauth2 should sign in' do
    OmniAuth.config.add_mock(:google_oauth2, omniauth_hash)
    post user_google_oauth2_omniauth_callback_url
    assert_redirected_to root_url
  end

  test '#google_oauth2 should not sign in' do
    post user_google_oauth2_omniauth_callback_url
    assert_redirected_to root_url
  end

  private

  def omniauth_hash
    {
      provider: 'google_oauth2',
      uid: '12345',
      info: {
        name: 'test',
        email: 'test3@test.com'
      }
    }
  end
end
