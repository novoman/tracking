require 'test_helper'
require 'json'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest

  test '#create should sign up' do
    post '/users', params: { user: user_correct_params }
    assert_response :success
    assert_equal 'Welcome! You have signed up successfully.', flash[:success]
  end

  test '#create should not sign up' do
    post '/users', params: { user: user_incorrect_params }
    assert_response :unauthorized
    assert_equal ['Email is invalid', "Password confirmation doesn't match Password"], JSON.parse(response.body)
  end

  test '#update should update user' do
    user = users(:user_001)
    sign_in user
    user.full_name = 'test2'
    user_attributes = user.attributes.clone.with_indifferent_access
    user_attributes[:current_password] = '111111'

    patch '/users', params: { user: user_attributes }
    assert_response :success
    assert_equal 'Your account has been updated successfully.', flash[:success]
  end

  test '#update should not update user' do
    user = users(:user_001)
    sign_in user

    patch '/users', params: { user: user_incorrect_params }
    assert_response :bad_request
    assert_equal ['Email is invalid', "Password confirmation doesn't match Password", "Current password can't be blank"], JSON.parse(response.body)
  end

  private

  def user_correct_params
    user = {}
    user[:full_name] = 'test'
    user[:email] = 'test1@test.com'
    user[:password] = '111111'
    user[:password_confirmation] = '111111'

    user
  end

  def user_incorrect_params
    user = {}
    user[:email] = 'test'
    user[:password] = '111111'
    user[:password_confirmation] = '222'

    user
  end

end
