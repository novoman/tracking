require 'test_helper'

class MoneyOperationControllerTest < ActionDispatch::IntegrationTest

  def setup
    sign_in users(:user_001)
  end

  test '#render_main_table' do
    post money_operation_render_main_table_url
    assert_response :ok
  end

  test '#render_add_modal' do
    post money_operation_render_add_modal_url
    assert_response :ok
  end

  test '#render_edit_modal' do
    money_operation = money_operations(:money_operation_001)

    post money_operation_render_edit_modal_url, params: { id: money_operation.id }
    assert_response :ok
  end

  test '#render_chart_modal' do
    post money_operation_render_chart_modal_url
    assert_response :ok
  end

  test '#ajax_add_money_operation' do
    post money_operation_ajax_add_money_operation_url, params: { money_operation: money_operation_params }
    assert_response :ok
  end

  test '#ajax_edit_money_operation' do
    money_operation = money_operations(:money_operation_001)
    money_operation.sum = 2000
    money_operation.comment = 'test2'
    money_operation.operation_date = Date.today
    money_operation_attributes = money_operation.attributes.clone.with_indifferent_access

    post money_operation_ajax_edit_money_operation_url, params: { money_operation: money_operation_attributes }
    assert_response :ok
  end

  test '#ajax_delete_money_operation' do
    money_operation = money_operations(:money_operation_001)

    post money_operation_ajax_delete_money_operation_url, params: { money_operation: { id: money_operation.id } }
    assert_response :ok
  end

  test '#export' do
    get '/money_operation/export.csv'
    assert_response :success
  end

  test '#income_chart' do
    get money_operation_income_chart_url
    assert_response :success
  end

  test '#expenses_chart' do
    get money_operation_expenses_chart_url
    assert_response :success
  end

  private

  def money_operation_params
    money_operation = {}
    money_operation[:sum] = 1000
    money_operation[:comment] = 'test1'
    money_operation[:operation_date] = '2019-06-11'
    money_operation[:category_id] = 1

    money_operation
  end

end
