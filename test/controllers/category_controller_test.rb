require 'test_helper'

class CategoryControllerTest < ActionDispatch::IntegrationTest

  def setup
    sign_in users(:user_001)
  end

  test '#ajax_add_category' do
    post category_ajax_add_category_url, params: { category: category_params }
    assert_response :ok
  end

  test '#ajax_edit_category' do
    category = categories(:category_001)
    category.name = 'test2'
    category_attributes = category.attributes.clone.with_indifferent_access

    post category_ajax_edit_category_url, params: { category: category_attributes }
    assert_response :ok
  end

  test '#ajax_delete_category' do
    category = categories(:category_001)

    post category_ajax_delete_category_url, params: { category: { id: category.id } }
    assert_response :ok
  end

  private

  def category_params
    category = {}
    category[:category_type_id] = 1
    category[:name] = 'test1'

    category
  end

end
