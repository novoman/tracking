require 'test_helper'
require 'will_paginate/array'

class ApplicationHelperTest < ActionView::TestCase
  attr_reader :user

  def setup
    @user = users(:user_001)
  end

  test '#page_entries_info showing all' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    money_operations = MoneyOperation::Finder.new(user, params).find

    assert_equal 'Showing all', page_entries_info(money_operations)
  end

  test '#page_entries_info no items found' do
    params = {
      start_date: '2019-05-30',
      end_date: '2019-05-30'
    }
    money_operations = MoneyOperation::Finder.new(user, params).find

    assert_equal 'No items found', page_entries_info(money_operations)
  end

  test '#page_entries_info showing one page' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    money_operations = []
    6.times { money_operations << MoneyOperation::Finder.new(user, params).find }

    assert_equal 'Showing <b>1</b>-<b>5</b> of <b>6</b> entries', page_entries_info(money_operations.paginate(page: 1, per_page: 5))
  end

end
