require 'test_helper'

class MoneyOperation::CreatorTest < ActiveSupport::TestCase
  attr_reader :category

  def setup
    @category = categories(:category_001)
  end

  test '#create' do
    money_operation_params = ActionController::Parameters.new(
      sum: 1000,
      comment: 'Test',
      operation_date: Date.today,
      category_id: category[:id]
    )
    MoneyOperation::Creator.new(money_operation_params).create
  end
end

