require 'test_helper'

class MoneyOperation::EditorTest < ActiveSupport::TestCase
  attr_reader :category, :money_operation

  def setup
    @category = categories(:category_001)
    @money_operation = money_operations(:money_operation_001)
  end

  test '#edit' do
    money_operation_params = ActionController::Parameters.new(
      id: money_operation[:id],
      sum: 1000,
      comment: 'Test',
      operation_date: Date.today,
      category_id: category[:id]
    )
    MoneyOperation::Editor.new(money_operation_params).edit
  end
end

