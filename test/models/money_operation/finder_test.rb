require 'test_helper'

class MoneyOperation::FinderTest < ActiveSupport::TestCase
  attr_reader :user

  def setup
    @user = users(:user_001)
  end

  test '#find' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    MoneyOperation::Finder.new(user, params).find
  end

  test '#find_for_export' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    MoneyOperation::Finder.new(user, params).find_for_export
  end

  test '#find_for_chart' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    MoneyOperation::Finder.new(user, params).find_for_chart('Income')
  end
end

