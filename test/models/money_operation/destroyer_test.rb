require 'test_helper'

class MoneyOperation::DestroyerTest < ActiveSupport::TestCase
  attr_reader :money_operation

  def setup
    @money_operation = money_operations(:money_operation_001)
  end

  test '#destroy' do
    money_operation_params = { id: money_operation[:id].to_s }
    MoneyOperation::Destroyer.new(money_operation_params).destroy
  end
end

