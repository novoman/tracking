require 'test_helper'

class MoneyOperation::ExporterTest < ActiveSupport::TestCase
  attr_reader :user

  def setup
    @user = users(:user_001)
  end

  test '#to_csv' do
    params = {
      start_date: '2019-05-28',
      end_date: '2019-05-30'
    }
    money_operations = MoneyOperation::Finder.new(user, params).find_for_export

    MoneyOperation::Exporter.new(money_operations).to_csv
  end
end

