require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test '#self.from_omniauth' do
    User.from_omniauth(omniauth_hash)
  end

  private

  def omniauth_hash
    OmniAuth::AuthHash.new(
      provider: 'google_oauth2',
      uid: '12345',
      info: {
        name: 'test',
        email: 'test3@test.com'
      }
    )
  end
end
