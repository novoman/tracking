require 'test_helper'

class Category::EditorTest < ActiveSupport::TestCase
  attr_reader :user, :category

  def setup
    @user = users(:user_001)
    @category = categories(:category_001)
  end

  test '#edit' do
    category_params = ActionController::Parameters.new(name: 'test2', id: category[:id])
    Category::Editor.new(category_params, user).edit
  end
end

