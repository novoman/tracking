require 'test_helper'

class Category::DestroyerTest < ActiveSupport::TestCase
  attr_reader :user, :category

  def setup
    @user = users(:user_001)
    @category = categories(:category_001)
  end

  test '#destroy' do
    category_params = ActionController::Parameters.new(id: category[:id])
    Category::Destroyer.new(category_params, user).destroy
  end
end

