require 'test_helper'

class Category::CreatorTest < ActiveSupport::TestCase
  attr_reader :user, :category_type

  def setup
    @user = users(:user_001)
    @category_type = category_types(:category_type_001)
  end

  test '#create' do
    category_params = ActionController::Parameters.new(name: 'test1', category_type_id: category_type[:id])
    Category::Creator.new(category_params, user).create
  end
end

