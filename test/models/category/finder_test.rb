require 'test_helper'

class Category::FinderTest < ActiveSupport::TestCase
  attr_reader :user, :category_type

  def setup
    @user = users(:user_001)
    @category_type = category_types(:category_type_001)
  end

  test '#find' do
    Category::Finder.new(category_type, user).find
  end
end

