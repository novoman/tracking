class CategoryController < ApplicationController
  respond_to :json

  def ajax_add_category
    self.ajax_action = true
    category = Category::Creator.new(params[:category], current_user).create

    render json: { category_id: category.id }, status: :ok
  end

  def ajax_edit_category
    self.ajax_action = true
    category = Category::Editor.new(params[:category], current_user).edit

    return render json: { error: 'Wrong category is to be edited' }, status: :not_found unless category

    render json: {}, status: :ok
  end

  def ajax_delete_category
    self.ajax_action = true
    result = Category::Destroyer.new(params[:category], current_user).destroy

    return render json: { error: 'Wrong category is to be edited' }, status: :not_found unless result

    render json: {}, status: :ok
  end

end
