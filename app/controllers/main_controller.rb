class MainController < ApplicationController
  def index
    @money_operations = MoneyOperation::Finder.new(current_user, params).find
    @income_categories = Category::Finder.new(current_user, CategoryType.find_by(name: 'Income')).find
    @expenses_categories = Category::Finder.new(current_user, CategoryType.find_by(name: 'Expenses')).find
  end
end
