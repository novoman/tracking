class MoneyOperationController < ApplicationController
  respond_to :json

  def render_main_table
    self.ajax_action = true
    @money_operations = money_operations(params)

    render partial: 'main/main_table', status: :ok
  end

  def render_add_modal
    self.ajax_action = true

    render partial: 'main/money_operations_add_modal', status: :ok
  end

  def render_edit_modal
    self.ajax_action = true
    @money_operation = money_operations(params)

    render partial: 'main/money_operations_edit_modal', status: :ok
  end

  def render_chart_modal
    self.ajax_action = true

    render partial: 'main/money_operations_chart_modal', status: :ok
  end

  def ajax_add_money_operation
    self.ajax_action = true
    MoneyOperation::Creator.new(params[:money_operation]).create
    @money_operations = money_operations(params)

    render partial: 'main/main_table', status: :ok
  end

  def ajax_edit_money_operation
    self.ajax_action = true

    MoneyOperation::Editor.new(params[:money_operation]).edit
    @money_operations = money_operations(params)

    render partial: 'main/main_table', status: :ok
  end

  def ajax_delete_money_operation
    self.ajax_action = true
    MoneyOperation::Destroyer.new(params[:money_operation]).destroy
    @money_operations = money_operations(params)

    render partial: 'main/main_table', status: :ok
  end

  def export
    @money_operations = MoneyOperation::Finder.new(current_user, params).find_for_export

    respond_to do |format|
      format.csv { send_data MoneyOperation::Exporter.new(@money_operations).to_csv }
      format.xls { render 'money_operation/xml_main_table' }
    end
  end

  def income_chart
    render json: MoneyOperation::Finder.new(current_user, params).find_for_chart('Income')
  end

  def expenses_chart
    render json: MoneyOperation::Finder.new(current_user, params).find_for_chart('Expenses')
  end

  private

  def money_operations(params = {})
    MoneyOperation::Finder.new(current_user, params).find
  end

end
