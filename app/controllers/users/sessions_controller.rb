class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  respond_to :json

  def create
    resource = User.find_for_database_authentication(email: params[:user][:email])

    if resource&.valid_password?(params[:user][:password])
      sign_in :user, resource
      set_flash_message(:success, :signed_in)
      return render json: flash[:success], status: :ok
    end

    invalid_login_attempt(resource)
  end

  private

  def invalid_login_attempt(resource)
    error_messages = []

    if resource&.errors&.any?
      resource.errors.full_messages.each do |message|
        error_messages << message
      end
    else
      error_messages << 'Invalid email or password.'
    end

    render json: error_messages, status: :unauthorized
  end

end
