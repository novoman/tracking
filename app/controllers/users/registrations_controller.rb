# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  respond_to :json

  def create
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      sign_up(resource_name, resource)
      set_flash_message! :success, :signed_up
      render json: flash[:success], status: :ok
    else
      clean_up_passwords resource
      set_minimum_password_length
      invalid_attempt('sign_up')
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      set_flash_message_for_update(resource, prev_unconfirmed_email)
      bypass_sign_in resource, scope: resource_name if sign_in_after_change_password?
      set_flash_message! :success, :updated
      render json: flash[:success], status: :ok
    else
      clean_up_passwords resource
      set_minimum_password_length
      invalid_attempt('update')
    end
  end

  private

  def invalid_attempt(type)
    error_messages = []

    if resource&.errors&.any?
      resource.errors.full_messages.each do |message|
        error_messages << message
      end
    else
      error_messages << 'Unknown error.'
    end

    render json: error_messages, status: type == 'sign_up' ? :unauthorized : :bad_request
  end

  def sign_up_params
    params.require(:user).permit(:full_name, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:full_name, :email, :password, :password_confirmation, :current_password)
  end

end
