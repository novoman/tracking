$(document).ready(function () {

  // categories for select
  var categories_select;

  // Datepicker setup
  var datepicker_params = {
    type: 'date',
    orientation: 'PORTRAIT',
    prevHandle: '<',
    nextHandle: '>',
    autoClose: true
  };

  var datepicker = new mdDateTimePicker.default(datepicker_params);
  var date_input;
  set_money_operations_date_inputs();

  $(".img-dollar").mouseenter(function () {
    $(".button-hint div").css("visibility", "visible");
  });

  $(".img-dollar").mouseleave(function () {
    $(".button-hint div").css("visibility", "hidden");
  });

  $(document).click(function(e) {
    var elem = $(e.toElement);
    if (!elem.hasClass("mddtp-picker") && !elem.hasClass("operation-date-input") && !elem.hasClass("calendar-img") && !$(elem).parent().parents(".mddtp-picker--portrait").length) {
      if (datepicker.isOpen()) datepicker.hide();
    }
  });

  $(document).on("click", "#operation_date, #operation_date_start, #operation_date_end", function () {
    date_input = $(this);
    datepicker.show();
  });

  $(document).on("click", ".calendar-img", function () {
    date_input = $(this).prev("input");
    datepicker.show();
  });

  $(document).on("click", ".mddtp-picker__cell", function () {
    var date_start = new Date();
    var date_end = new Date();
    var wrong_date_flag = false;

    if (date_input.attr("id") == "operation_date_start" && $("#operation_date_end").val() != "") {
      date_start = new Date(datepicker.time);
      date_end = new Date($("#operation_date_end").val());
      if (date_start.setHours(0,0,0,0) > date_end.setHours(0,0,0,0)) {
        $("#alert-modal div.modal-body").text("Start date can't be greater than end date! So we've changed it to max possible.");
        $("#alert-modal .modal-title").text("Alert!");
        $("#alert-modal").modal("show");
        date_input.val(date_end.yyyymmdd());
        wrong_date_flag = true;
      }
    }

    if (date_input.attr("id") == "operation_date_end" && $("#operation_date_start").val() != "") {
      date_start = new Date($("#operation_date_start").val());
      date_end = new Date(datepicker.time);
      if (date_end.setHours(0,0,0,0) < date_start.setHours(0,0,0,0)) {
        $("#alert-modal div.modal-body").text("End date can't be less than start date! So we've changed it to min possible.");
        $("#alert-modal .modal-title").text("Alert!");
        $("#alert-modal").modal("show");
        date_input.val(date_start.yyyymmdd());
        wrong_date_flag = true;
      }
    }

    if (!wrong_date_flag) {
      var date = new Date(datepicker.time);
      date_input.val(date.yyyymmdd());
    }

    if ($("#operation_date_start").val() != "" && $("#operation_date_end").val() != "") {
      refresh_money_operation_table();
    }
  });

  // Activate tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // Only numbers in Sum input
  $(document).on("keypress", "input#sum", function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && e.which != 45 && e.which != 46 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });

  // Select/Deselect checkboxes
  $(document).on("click", "#select-all-checkboxes", function () {
    var checkbox = $("#main-table").find('table tbody input[type="checkbox"]');

    if (this.checked) {
      checkbox.each(function () {
        this.checked = true;
      });
    } else {
      checkbox.each(function () {
        this.checked = false;
      });
    }
  });
  $(document).on("click", "#main-table table tbody input[type=\"checkbox\"]", function () {
    if (!this.checked) {
      $("#select-all-checkboxes").prop("checked", false);
    }
  });

  $(document).on("click", "#main-table-section .add-new-operation", function (e) {
    e.preventDefault();

    $.ajax({
      type: "POST",
      url: 'money_operation/render_add_modal',
      beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
      }
    }).done(function (response) {
      $("#add-operation-modal .modal-content").html(response);
      categories_select = $("#add-operation-modal #money_operation_category_id").html();
      $(".money_operation_category_type").val($("#category_type_id").val()).trigger('change');
      $("#add-operation-modal").modal("show");
    }).fail(function (response) {
      $("#alert-modal div.modal-body").text(response.responseText);
      $("#alert-modal .modal-title").text("Error!");
      $("#alert-modal").modal("show");
    });
  });

  $(document).on("click", "#main-table-section .delete-selected", function () {
    var selected_checkboxes = $("#main-table table tbody input[type=\"checkbox\"]:checked");
    var checked_ids = [];

    if (selected_checkboxes.length) {
      $.each(selected_checkboxes, function (key, checkbox) {
        checked_ids.push($(checkbox).val());
      });
      $("#delete-operation-modal .money-operation-id").val(checked_ids);
      $("#delete-operation-modal").modal("show");
    } else {
      $("#alert-modal div.modal-body").text("You didn't choose any money operation!");
      $("#alert-modal .modal-title").text("Error!");
      $("#alert-modal").modal("show");
    }

  });

  $(document).on("click", "#main-table .edit", function (e) {
    e.preventDefault();
    var money_operation_id = $(this).parents("tr").find("#money_operation_id").val();
    var category_type_id = $(this).parents("tr").find("#category_type_id").val();

    $.ajax({
      type: "POST",
      url: 'money_operation/render_edit_modal',
      data: { id: money_operation_id },
      beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
      }
    }).done(function (response) {
      $("#edit-operation-modal .modal-content").html(response);
      categories_select = $("#edit-operation-modal #money_operation_category_id").html();
      $(".money_operation_category_type").val(category_type_id).trigger('change');
      $("#edit-operation-modal").modal("show");
    }).fail(function (response) {
      $("#alert-modal div.modal-body").text(response.responseText);
      $("#alert-modal .modal-title").text("Error!");
      $("#alert-modal").modal("show");
    });
  });

  $(document).on("click", "form#new_money_operation .add-new", function (e) {
    e.preventDefault();

    var empty = false;
    var input = $(this).parents("#new_money_operation").find('input[type="text"]');
    input.each(function () {
      if (!$(this).val()) {
        $(this).addClass("error");
        empty = true;
      } else {
        $(this).removeClass("error");
      }
    });
    $(this).parents("#new_money_operation").find(".error").first().focus();

    if (!empty) {
      var start_date = $("#operation_date_start").val();
      var end_date = $("#operation_date_end").val();
      var data = $("form#new_money_operation").serializeArray();
      data.push({ name: 'start_date', value: start_date });
      data.push({ name: 'end_date', value: end_date });

      $.ajax({
        type: "POST",
        url: 'money_operation/ajax_add_money_operation',
        data: data,
        beforeSend: function (request) {
          return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        }
      }).done(function (response) {
        $("#main-table").html(response);
        $("#add-operation-modal").modal('toggle');
        $("#new_money_operation").each(function () {
          this.reset();
        });
        $("#new_money_operation .add-error").remove();
      }).fail(function (response) {
        $("#new_money_operation div.modal-body").append(
          "<div class='add-error'>" +
          "Error! " + response.responseText.substring(0, 200) +
          "</div>"
        );
      });
    }
  });

  $(document).on("click", "form.edit_money_operation .edit-button", function (e) {
    e.preventDefault();

    var empty = false;
    var input = $(this).parents("form.edit_money_operation").find('input[type="text"]');
    input.each(function () {
      if (!$(this).val()) {
        $(this).addClass("error");
        empty = true;
      } else {
        $(this).removeClass("error");
      }
    });
    $(this).parents("form.edit_money_operation").find(".error").first().focus();

    if (!empty) {
      var start_date = $("#operation_date_start").val();
      var end_date = $("#operation_date_end").val();
      var data = $("form.edit_money_operation").serializeArray();
      data.push({ name: 'start_date', value: start_date });
      data.push({ name: 'end_date', value: end_date });

      $.ajax({
        type: "POST",
        url: 'money_operation/ajax_edit_money_operation',
        data: data,
        beforeSend: function (request) {
          return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        }
      }).done(function (response) {
        $("#main-table").html(response);
        $("#edit-operation-modal").modal('toggle');
        $("form.edit_money_operation").each(function () {
          this.reset();
        });
        $("form.edit_money_operation .edit-error").remove();
      }).fail(function (response) {
        $("form.edit_money_operation div.modal-body").append(
          "<div class='edit-error'>" +
          "Error! " + response.responseText.substring(0, 200) +
          "</div>"
        );
      });
    }
  });

  $(document).on("click", "#main-table .delete", function () {
    $("#delete-operation-modal .money-operation-id").val($(this).parents("tr").find("#money_operation_id").val());
    $("#delete-operation-modal").modal("show");
  });

  $(document).on("click", "#delete-operation-modal .delete-button", function (e) {
    e.preventDefault();
    var money_operation_id = $(this).parents("#delete-operation-modal").find(".money-operation-id").val();
    var money_operation = [];
    money_operation["id"] = money_operation_id;
    var start_date = $("#operation_date_start").val();
    var end_date = $("#operation_date_end").val();

    $.ajax({
      type: "POST",
      url: 'money_operation/ajax_delete_money_operation',
      data: { money_operation: $.extend({}, money_operation), start_date: start_date, end_date: end_date },
      beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
      }
    }).done(function (response) {
      $("#main-table").html(response);
      $("#delete-operation-modal").modal('toggle');
      $("#delete-operation-modal .delete-error").remove();
    }).fail(function (response) {
      $("#delete-operation-modal div.modal-body").append(
        "<div class='delete-error'>" +
        "Error! " + response.responseText.substring(0, 200) +
        "</div>"
      );
    });
  });

  $(document).on("change", ".money_operation_category_type", function () {
    var category_type = $(this).parents(".modal-body").find(".money_operation_category_type :selected").text();
    var options = $(categories_select).filter("optgroup[label='" + category_type + "']").html();
    if (options) {
      $(this).parents(".modal-body").find("#money_operation_category_id").html(options);
      $(this).parents(".modal-body").find("#money_operation_category_id").parent().show();
    } else {
      $(this).parents(".modal-body").find("#money_operation_category_id").empty();
      $(this).parents(".modal-body").find("#money_operation_category_id").parent().hide();
    }
  });

  $(document).on("click", "#main-table .pagination a", function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var page = getURLParameter(url, 'page');
    var start_date = $("#operation_date_start").val();
    var end_date = $("#operation_date_end").val();

    $.ajax({
      type: "POST",
      url: 'money_operation/render_main_table',
      data: { page: page, start_date: start_date, end_date: end_date },
      beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
      }
    }).done(function (response) {
      $("#main-table").html(response);
    }).fail(function (response) {
      $("#alert-modal div.modal-body").text(response.responseText);
      $("#alert-modal .modal-title").text("Error!");
      $("#alert-modal").modal("show");
    });
  });

  $(document).on("click", "#main-table .show-chart", function (e) {
    e.preventDefault();
    var start_date = $("#operation_date_start").val();
    var end_date = $("#operation_date_end").val();

    $.ajax({
      type: "POST",
      url: 'money_operation/render_chart_modal',
      data: { start_date: start_date, end_date: end_date },
      beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
      }
    }).done(function (response) {
      $("#chart-modal .modal-body").html(response);
      $("#chart-modal").modal("show");
    }).fail(function (response) {
      $("#alert-modal div.modal-body").text(response.responseText);
      $("#alert-modal .modal-title").text("Error!");
      $("#alert-modal").modal("show");
    });
  });

  function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
  }

  function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
      tempArray = additionalURL.split("&");
      for (var i=0; i<tempArray.length; i++){
        if(tempArray[i].split('=')[0] != param) {
          newAdditionalURL += temp + tempArray[i];
          temp = "&";
        }
      }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
  }

  function set_money_operations_date_inputs() {
    var current_date = new Date();
    $("#operation_date_end").val(current_date.yyyymmdd());

    var date_week_ago = current_date.getDate() - 7;
    current_date.setDate(date_week_ago);
    $("#operation_date_start").val(current_date.yyyymmdd());
  }

});

function refresh_money_operation_table() {
  var start_date = $("#operation_date_start").val();
  var end_date = $("#operation_date_end").val();

  $.ajax({
    type: "POST",
    url: 'money_operation/render_main_table',
    data: { start_date: start_date, end_date: end_date },
    beforeSend: function (request) {
      return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    }
  }).done(function (response) {
    $("#main-table").html(response);
  }).fail(function (response) {
    $("#alert-modal div.modal-body").text(response.responseText);
    $("#alert-modal .modal-title").text("Error!");
    $("#alert-modal").modal("show");
  });
}
