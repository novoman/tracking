$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();

  // Append table with add row form on add new button click
  $("#categories").find(".add-new").click(function () {
    var empty_tr = $(this).parents(".table-wrapper").find("table tbody tr.empty");
    if (empty_tr) empty_tr.remove();

    $(this).attr("disabled", "disabled");
    var index = $(this).parents(".table-wrapper").find("table tbody tr:last-child").index();
    var row = '<tr>' +
                '<td><input type="text" class="form-control category-input" name="name"></td>' +
                '<td>' + build_actions_td($(this).attr("data-category-type-id")) + '</td>' +
              '</tr>';
    $(this).parents(".table-wrapper").find("table").append(row);
    $(this).parents(".table-wrapper").find("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
    $(this).parents(".table-wrapper").find("table tbody tr").eq(index + 1).find("input").focus();
    $('[data-toggle="tooltip"]').tooltip();
  });

  // Save edited field by pressing Enter
  $('body').on("keyup", "input.category-input", function(e) {
    if (e.keyCode === 13) {
      $(this).parents("tr").find(".add").click();
    }
  });

  // Add row on add button click
  $(document).on("click", "#categories .add", function () {
    var empty = false;
    var input = $(this).parents("tr").find('input[type="text"]');
    input.each(function () {
      if (!$(this).val()) {
        $(this).addClass("error");
        empty = true;
      } else {
        $(this).removeClass("error");
      }
    });
    $(this).parents("tr").find(".error").first().focus();
    var category_type_id = $(this).parents("tr").find("#category_type_id").val();
    var category_id = $(this).parents("tr").find("#category_id").val();

    if (!empty) {
      input.each(function () {
        // trim spaces from category_name
        $(this).val($.trim($(this).val()));

        var url = "";
        var category_name = $(this).val();

        var category = [];
        category["name"] = category_name;
        category["category_type_id"] = category_type_id;

        var add_new_category_condition = $(this).parents("tr").is($(this).parents(".table-wrapper").find("table tbody tr:last-child")) &&
          $(this).parents(".table-wrapper").find(".add-new").attr("disabled") == "disabled";

        if (add_new_category_condition) {
          url = "category/ajax_add_category";
        } else {
          url = "category/ajax_edit_category";
          category["id"] = category_id;
        }
        var td = $(this).parent("td");
        var tr = $(this).parents("tr");

        $.ajax({
          type: "POST",
          url: url,
          data: { category: $.extend({}, category) },
          beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
          }
        }).done(function (response) {
          td.html(category_name);
          tr.parents(".table-wrapper").find(".add-new").removeAttr("disabled");
          tr.find(".add, .edit").toggle();
          if (add_new_category_condition) tr.find("#category_id").val(response.category_id);
          refresh_money_operation_table();
        }).fail(function (response) {
          var alert_msg = "";
          if (response.responseText.indexOf("ActiveRecord::RecordNotUnique") >= 0) alert_msg = "This category already exists!";
          else alert_msg = response.responseText;

          $("#alert-modal div.modal-body").text(alert_msg);
          $("#alert-modal").modal("show");
        });
      });
    }
  });

  // Edit row on edit button click
  $(document).on("click", "#categories .edit", function () {
    $(this).parents("tr").find("td:not(:last-child)").each(function () {
      $(this).html('<input type="text" class="form-control category-input" value="' + $(this).text() + '">');
      $(this).find("input").focus();
    });
    $(this).parents("tr").find(".add, .edit").toggle();
  });

  // Delete row on delete button click
  $(document).on("click", "#delete-category-modal .delete-button", function () {
    var category_id = $(this).parents("#delete-category-modal").find(".category-id").val();
    var category = [];
    category["id"] = category_id;

    if (category_id) {
      $.ajax({
        type: "POST",
        url: 'category/ajax_delete_category',
        data: { category: $.extend({}, category) },
        beforeSend: function (request) {
          return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        }
      }).done(function () {
        $("#delete-category-modal .delete-error").remove();
        refresh_money_operation_table();
      }).fail(function (response) {
        $("#delete-category-modal div.modal-body").append(
          "<div class='delete-error'>" +
          "Error! " + response.responseText.substring(0, 200) +
          "</div>"
        );
      });
    }

    $("#delete-category-modal").modal('toggle');
    var category_table = $("#category_id[value='"+ category_id + "']").parents(".table-wrapper");
    category_table.find(".add-new").removeAttr("disabled");
    $("#category_id[value='"+ category_id + "']").parents("tr").remove();

    if (category_table.find("table tbody tr").length == 0) {
      var row = '<tr class="empty">' +
                  '<td colspan="2">No categories</td>' +
                '</tr>';
      category_table.find("table").append(row);
    }
  });

  $(document).on("click", "#categories .delete", function () {
    $("#delete-category-modal .category-id").val($(this).parents("tr").find("#category_id").val());
    $("#delete-category-modal").modal("show");
  });

  function build_actions_td(categoty_type = 1) {
    return '<input type="hidden" name="category_id" id="category_id" value="">' +
      '<input type="hidden" name="category_type_id" id="category_type_id" value="' + categoty_type + '">' +
      '<a class="add" title="" data-toggle="tooltip" data-original-title="Add"><i class="material-icons"></i></a>' +
      '<a class="edit" title="" data-toggle="tooltip" data-original-title="Edit"><i class="material-icons"></i></a>' +
      '<a class="delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="material-icons"></i></a>';
  }
});
