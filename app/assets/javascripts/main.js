(function($) {
  "use strict"; // Start of use strict

  // convert JS Date object to string yyyy-mm-dd
  Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
      (mm>9 ? '' : '0') + mm,
      (dd>9 ? '' : '0') + dd
    ].join('-');
  };

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 500) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Modal popup$(function () {
  $('.app-modal-item').magnificPopup({
    type: 'inline',
    preloader: false,
    modal: true,
    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it
    }
  });
  $(document).on('click', '.app-modal-dismiss', function(e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  $(document).click(function(e) {
    var elem = $(e.toElement);
    if ((elem.hasClass("app-modal") && !elem.hasClass("app-modal-dialog")) || elem.hasClass("mfp-content")) {
      $.magnificPopup.close();
    }
  });

  function toggleResetPswd(e) {
    e.preventDefault();
    $('#logreg-forms .form-signin').toggle(); // display:block or none
    $('#logreg-forms .form-reset').toggle(); // display:block or none
  }

  function toggleSignUp(e) {
    e.preventDefault();
    $('#logreg-forms .form-signin').toggle(); // display:block or none
    $('#logreg-forms .form-signup').toggle(); // display:block or none
  }

  $(() => {
    // Login Register Form
    $('#logreg-forms #forgot_pswd').click(toggleResetPswd);
    $('#logreg-forms #cancel_reset').click(toggleResetPswd);
    $('#logreg-forms #btn-signup').click(toggleSignUp);
    $('#logreg-forms #cancel_signup').click(toggleSignUp);
  });

  $("form#sign_in_user, form#sign_up_user, form#edit_user").bind("ajax:success", function() {
    $.magnificPopup.close();
    window.location.reload();
    window.location.href = '#page-top';
  }).bind("ajax:error", function(event) {
    var error_messages = event.detail[2].responseText;
    error_messages = JSON.parse(error_messages);
    error_messages = error_messages.join("<br>");
    error_messages = "<div class='alert alert-danger pull-left'>" + error_messages + "</div>";

    $(this).children('.modal-errors').html(error_messages);
  });

})(jQuery); // End of use strict
