module ApplicationHelper
  def page_entries_info(collection, options = {})
    entry_name = options[:entry_name] || (
      if collection.empty?
        'item'
      else
        collection.first.class.name.split('::').last.titleize
      end)
    if collection.total_pages < 2
      case collection.size
      when 0 then "No #{entry_name.pluralize} found"
      else; 'Showing all'
      end
    else
      "Showing <b>#{collection.offset + 1}</b>-<b>#{collection.offset + collection.length}</b> of <b>#{collection.total_entries}</b> entries".html_safe
    end
  end
end
