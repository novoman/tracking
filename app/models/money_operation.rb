class MoneyOperation < ApplicationRecord
  belongs_to :category
  after_find :to_date

  def to_date
    self.operation_date = operation_date.to_date
  end
end
