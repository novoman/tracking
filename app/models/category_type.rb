class CategoryType < ApplicationRecord
  has_many :category

  def self.category_type_id(category_name)
    CategoryType.find_by(name: category_name)&.id
  end
end
