class MoneyOperation::Destroyer

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def destroy
    money_operations = MoneyOperation.find(params[:id].split(',').map(&:to_i))
    money_operations.each(&:destroy!)
  end

end
