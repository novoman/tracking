class MoneyOperation::Exporter

  attr_reader :money_operations

  def initialize(money_operations)
    @money_operations = money_operations
  end

  def to_csv
    CSV.generate do |csv|
      csv << export_columns
      money_operations.each do |money_operation|
        attribute_values = []
        attribute_values << money_operation.category.category_type[:name]
        attribute_values << money_operation.category[:name]
        attribute_values << money_operation[:sum]
        attribute_values << money_operation[:comment]
        attribute_values << money_operation[:operation_date].to_date

        csv << attribute_values
      end
    end
  end

  private

  def export_columns
    ['Category Type', 'Category', 'Sum', 'Comment', 'Operation Date']
  end
end
