class MoneyOperation::Finder

  attr_reader :user, :start_date, :end_date, :id, :page

  MONEY_OPERATIONS_PER_PAGE = 5

  def initialize(user, params = {})
    @user = user
    @id = params[:id]
    @page = params[:page]
    start_date = params[:start_date] || (Time.now - 7.days).strftime('%Y-%m-%d')
    end_date = params[:end_date] || Time.now.strftime('%Y-%m-%d')
    @start_date = Date.parse(start_date)
    @end_date = Date.parse(end_date)
  end

  def find
    return MoneyOperation.find(id) if id

    MoneyOperation
      .joins(:category)
      .where(categories: { user: user })
      .where(operation_date: start_date..end_date)
      .paginate(page: page, per_page: MONEY_OPERATIONS_PER_PAGE)
      .order(operation_date: :desc)
  end

  def find_for_export
    MoneyOperation
      .joins(:category)
      .where(categories: { user: user })
      .where(operation_date: start_date..end_date)
      .order(operation_date: :desc)
  end

  def find_for_chart(category_type)
    MoneyOperation
      .joins(category: :category_type)
      .where(categories: { user: user })
      .where(category_types: { name: category_type })
      .where(operation_date: start_date..end_date)
      .group('categories.name')
      .sum(:sum)
  end
end
