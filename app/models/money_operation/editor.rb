class MoneyOperation::Editor

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def edit
    money_operation = MoneyOperation.find(params[:id])
    money_operation.update_attributes!(permitted_params)

    money_operation
  end

  private

  def permitted_params
    params.permit(:sum, :comment, :operation_date, :category_id)
  end

end
