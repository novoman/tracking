class MoneyOperation::Creator

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def create
    money_operation = MoneyOperation.new(permitted_params)
    money_operation.save!

    money_operation
  end

  private

  def permitted_params
    params.permit(:sum, :comment, :operation_date, :category_id)
  end

end
