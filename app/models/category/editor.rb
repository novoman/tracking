class Category::Editor

  attr_reader :params, :user

  def initialize(params, user)
    @params = params
    @user = user
  end

  def edit
    category = Category.find_by(id: params[:id], user: user)
    return false unless category

    category.update_attributes(permitted_params)
    category.save!

    category
  end

  private

  def permitted_params
    params.permit(:name)
  end

end
