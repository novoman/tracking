class Category::Finder

  attr_reader :user, :categories_type

  def initialize(user, category_type = nil)
    @user = user
    @categories_type = category_type || CategoryType.all.ids
  end

  def find
    Category.where(user: user, category_type: categories_type)
  end
end
