class Category::Destroyer

  attr_reader :params, :user

  def initialize(params, user)
    @params = params
    @user = user
  end

  def destroy
    category = Category.find_by(id: params[:id], user: user)
    return false unless category

    category.destroy!
  end

end
