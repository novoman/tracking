class Category::Creator

  attr_reader :params, :user, :category_type

  def initialize(params, user)
    @params = params
    @user = user
    @category_type = CategoryType.find(params[:category_type_id])
  end

  def create
    category = Category.new(permitted_params)
    category.user = user
    category.category_type = category_type
    category.save!

    category
  end

  private

  def permitted_params
    params.permit(:name)
  end

end
