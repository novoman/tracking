class Category < ApplicationRecord
  belongs_to :user
  belongs_to :category_type
  has_many :money_operation

  def self.for_select(user)
    CategoryType.all.map do |category_type|
      [category_type.name, where(category_type: category_type, user: user).map { |c| [c.name, c.id] }]
    end
  end
end
